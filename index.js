const app = require('express')();
const bodyParser = require('body-parser');
const Web3 = require('web3');
const Eos = require('eosjs');
const TronWeb = require('tronweb');
const StellarSdk = require('stellar-sdk');
const RippleAPI = require('ripple-lib').RippleAPI;
const { default: Neon, api, wallet } = require("@cityofzion/neon-js");

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

// --------------- Transfer -----------------

app.post('/ETH', function (req, resp) {
    if (!req.body.from || !req.body.to || !req.body.value)
       																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																																 resp.status(400).json({ Error: "Bad Request" });

    else {
        ETH_config().then((web3) => {
            var tx = {
                to: req.body.to,
                gas: 550000,
                value: web3.utils.toWei(req.body.value, "ether")
            }

            web3.eth.accounts.signTransaction(tx, req.body.from, function (err, res) {
                if (err)
                    resp.status(400).json({ Error: err });

                web3.eth.sendSignedTransaction(res.rawTransaction).on('transactionHash', txHash => {
                    console.log("ETH tx-Hash", txHash);
                    resp.status(200).json({ "ETH tx-Hash": txHash });
                }).catch(err => {
                    resp.status(400).json({ Error: err });
                });
            });
        });
    }
});

app.post('/EOS', function (req, resp) {
    if (!req.body.pKey || !req.body.fromAcc || !req.body.toAcc || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        EOS_config(req.body.pKey, req.body.fromAcc).then((eos) => {
            eos.transfer(req.body.fromAcc, req.body.toAcc, req.body.value, req.body.memo).then(res => {
                console.log("EOS tx-Hash", res.transaction_id)
                resp.status(200).json({ "Tx-Hash": res.transaction_id, "EOS tx-Recipt": res });
            }).catch(err => {
                resp.status(400).json({ Error: err });
            });
        })
    }
});

app.post('/TRON', function (req, resp) {
    if (!req.body.pKey || !req.body.toAdd || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        TRON_config().then((tronWeb) => {
            tronWeb.trx.sendTransaction(req.body.toAdd, req.body.value, req.body.pKey, (err, res) => {
                if (err)
                    resp.status(400).json({ Error: err });

                console.log("TRON tx-Hash", res.transaction.txID)
                resp.status(200).json({ "Tx-Hash": res.transaction.txID, "TRON tx-Recipt": res });
            }).catch(err => {
                resp.status(400).json({ Error: err });
            });
        })
    }
});

app.post('/XLM', function (req, resp) {
    if (!req.body.pKey || !req.body.toAdd || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        XLM_config().then((server) => {
            var keyPair = StellarSdk.Keypair.fromSecret(req.body.pKey);
            server.loadAccount(req.body.toAdd)
                .catch(StellarSdk.NotFoundError, function (error) {
                    throw new Error('The destination account does not exist!');
                })
                .then(function () {
                    return server.loadAccount(keyPair.publicKey());
                })
                .then(function (sourceAccount) {
                    transaction = new StellarSdk.TransactionBuilder(sourceAccount)
                        .addOperation(StellarSdk.Operation.payment({
                            destination: req.body.toAdd,
                            asset: StellarSdk.Asset.native(),
                            amount: req.body.value
                        }))
                        .addMemo(StellarSdk.Memo.text('Test Transaction'))
                        .setTimeout(500)
                        .build();
                    transaction.sign(keyPair);
                    return server.submitTransaction(transaction);
                })
                .then(function (result) {
                    console.log('Steller tx-Hash:', result.hash);
                    resp.status(200).json({ "Tx-Hash": result.hash, "Steller tx-Recipt": result });
                })
                .catch(function (error) {
                    console.error('Something went wrong!', error);
                    resp.status(400).json({ Error: "Something went wrong!" });
                });
        })
    }
});

app.post('/XRP', function (req, resp) {
    if (!req.body.pKey || !req.body.fromAdd || !req.body.toAdd || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        const payment = {
            "source": {
                "address": req.body.fromAdd,
                "maxAmount": {
                    "value": req.body.value,
                    "currency": "XRP"
                }
            },
            "destination": {
                "address": req.body.toAdd,
                "amount": {
                    "value": req.body.value,
                    "currency": "XRP"
                }
            }
        };

        XRP_config().then((api) => {
            api.connect().then(() => {
                // console.log('Connected');
                return api.preparePayment(req.body.fromAdd, payment);
            }).then(prepared => {
                return api.getLedger().then(ledger => {
                    // console.log('Current Ledger', ledger.ledgerVersion);
                    return this.submitTransaction(ledger.ledgerVersion, prepared, req.body.pKey).then(result => {
                        console.log('Ripple tx-Hash:', result.tx_json.hash)
                        resp.status(200).json({ "Tx-Hash": result.tx_json.hash, "Ripple tx-Recipt": result });
                    });
                });
            }).then(() => {
                api.disconnect().then(() => {
                    // console.log('api disconnected');
                    // process.exit();
                });
            })
                .catch(function (error) {
                    console.error('Something went wrong!', error);
                    resp.status(400).json({ Error: "Something went wrong!" });
                });

            submitTransaction = (lastClosedLedgerVersion, prepared, secret) => new Promise(function (resolve, reject) {
                // function submitTransaction(lastClosedLedgerVersion, prepared, secret) {
                const signedData = api.sign(prepared.txJSON, secret);
                return api.submit(signedData.signedTransaction).then(data => {
                    // console.log('Tentative Result: ', data.resultCode);
                    // console.log('Tentative Message: ', data.resultMessage);
                    resolve(data)
                });
            })
        })
    }

});

app.post('/NEO', function (req, resp) {
    if (!req.body.pKey || !req.body.toAdd)
        resp.status(400).json({ Error: "Bad Request" });

    else if ((!req.body.NEO && !req.body.GAS) || Math.floor(req.body.NEO) !== req.body.NEO)
        resp.status(400).json({ Error: "Invalid Assets" });

    else {
        NEO_config().then(apiData => {

            var tx_asset = {}
            if (req.body.NEO) tx_asset.NEO = req.body.NEO
            if (req.body.GAS) tx_asset.GAS = req.body.GAS

            const config = {
                api: apiData,
                account: new wallet.Account(req.body.pKey),
                intents: api.makeIntent(tx_asset, req.body.toAdd)
            };

            console.log(tx_asset);

            Neon.sendAsset(config).then(result => {
                console.log('NEO tx-Hash:', result.response.txid)
                resp.status(200).json({ "Tx-Hash": result.response.txid, "NEO tx-Recipt": result });
            }).catch(function (error) {
                console.error('Something went wrong!', error);
                resp.status(400).json({ Error: "Something went wrong!" });
            });
        })
    }
});

app.post('/BTC', function (req, resp) {
    if (!req.body.pKey || !req.body.toAdd || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        BTC_config().then(btc => {
            var bitcore = btc.bitcore
            var tx = btc.trx

            var privateKeyWIF = req.body.pKey;
            var privateKey = bitcore.PrivateKey.fromWIF(privateKeyWIF);

            var sourceAddress = privateKey.toAddress(bitcore.Networks.testnet);

            BTC_explorer_config().then(ins => {
                ins.getUnspentUtxos(sourceAddress, function (error, utxos) {
                    if (error) {
                        resp.status(400).json({ Error: error });
                    } else {
                        // console.log(utxos);

                        tx.from(utxos);
                        tx.to(req.body.toAdd, req.body.value * 100000000);
                        tx.change(sourceAddress);
                        tx.sign(privateKey);

                        tx.serialize();

                        ins.broadcast(tx.toString(), function (error, transactionId) {
                            if (error) {
                                resp.status(400).json({ Error: error });
                            } else {
                                console.log({ "From_Adds": sourceAddress, "To_Adds": req.body.toAdd, "TxID": transactionId });
                                resp.status(200).json({ "From_Adds": sourceAddress, "To_Adds": req.body.toAdd, "TxID": transactionId });
                            }
                        });
                    }
                })
            })

        })
    }
});

app.post('/LTC', function (req, resp) {
    if (!req.body.pKey || !req.body.toAdd || !req.body.value)
        resp.status(400).json({ Error: "Bad Request" });

    else {
        BTC_config().then(btc => {
            var bitcore = btc.bitcore
            var tx = btc.trx

            var privateKeyWIF = req.body.pKey;
            var privateKey = bitcore.PrivateKey.fromWIF(privateKeyWIF);

            var sourceAddress = privateKey.toAddress(bitcore.Networks.testnet);

            LTC_explorer_config().then(ins => {
                ins.getUtxos(sourceAddress, function (error, utxos) {
                    if (error) {
                        resp.status(400).json({ Error: error });
                    } else {
                        // console.log(utxos);

                        tx.from(utxos);
                        tx.to(req.body.toAdd, req.body.value * 100000000);
                        tx.change(sourceAddress);
                        tx.sign(privateKey);

                        tx.serialize();

                        ins.broadcast(tx.toString(), function (error, transactionId) {
                            if (error) {
                                resp.status(400).json({ Error: error });
                            } else {
                                console.log({ "From_Adds": sourceAddress, "To_Adds": req.body.toAdd, "TxID": transactionId });
                                resp.status(200).json({ "From_Adds": sourceAddress, "To_Adds": req.body.toAdd, "TxID": transactionId });
                            }
                        });
                    }
                })
            })

        })
    }
});


// ------------- Get balances ---------------

app.get('/ETH/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    ETH_config().then((web3) => {
        web3.eth.getBalance(req.params.key, (err, balance) => {
            if (err)
                resp.status(400).json({ Error: err });

            console.log({ "Account": req.params.key, 'ETH Balance': balance + ' wei' });
            resp.status(200).json({ "Account": req.params.key, "ETH Balance": balance + ' wei' });
        }).catch(err => {
            resp.status(400).json({ Error: err });
        });
    });
});

app.get('/EOS/:acc', function (req, resp) {

    if (!req.params.acc)
        resp.status(400).json({ Error: "Bad Request" });

    EOS_config(null, null).then((eos) => {
        eos.getAccount(req.params.acc).then((res) => {
            console.log({ "Account": req.params.acc, "EOS Balance": res.core_liquid_balance });
            resp.status(200).json({ "Account": req.params.acc, "EOS Balance": res.core_liquid_balance, Account_Detail: res });
        }).catch(err => {
            resp.status(400).json({ Error: err });
        });
    });
});

app.get('/TRON/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    TRON_config().then((tronWeb) => {
        tronWeb.trx.getBalance(req.params.key, (err, balance) => {
            if (err)
                resp.status(400).json({ Error: err });

            console.log({ "Account": req.params.key, 'TRON Balance': balance });
            resp.status(200).json({ "Account": req.params.key, "TRON Balance": balance });
        }).catch(err => {
            resp.status(400).json({ Error: err });
        });
    }).catch(err => {
        // console.log(err)
    });
});

app.get('/XLM/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    XLM_config().then((server) => {
        server.loadAccount(req.params.key).then(function (account) {
            console.log({ "Account": req.params.key, "Steller Balance": account.balances });
            resp.status(200).json({ "Account": req.params.key, "Steller Balances": account.balances });
        }).catch(err => {
            resp.status(400).json({ Error: err });
        });
    });
});

app.get('/XRP/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    XRP_config().then((api) => {
        api.connect().then(() => {
            return api.getBalances(req.params.key);
        }).then(info => {
            console.log({ "Account": req.params.key, "Ripple Balance": info });
            resp.status(200).json({ "Account": req.params.key, "Ripple Balance": info });
        }).then(() => {
            api.disconnect().then(() => {
                // process.exit();
            });
        }).catch(err => {
            resp.status(400).json({ Error: err });
        });
    });
});

app.get('/NEO/:key', function (req, resp) {

    // resp.status(400).json({Message: "This method is temporarily unavailable"});

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    NEO_config().then(apiData => {
        apiData.getBalance(req.params.key).then(balance => {
            var assets = balance.assets;
            var gas_val = 0;
            var neo_val = 0;

            if (assets.GAS)
                for (var i = 0; i < assets.GAS.unspent.length; i++)
                    gas_val += parseFloat(assets.GAS.unspent[i].value);

            if (assets.NEO)
                for (var i = 0; i < assets.NEO.unspent.length; i++)
                    neo_val += parseInt(assets.NEO.unspent[i].value);

            console.log({ "Account": req.params.key, "NEO Balance": neo_val + " NEO", "GAS Balance": gas_val.toFixed(8) + " GAS" });
            resp.status(200).json({ "Account": req.params.key, "NEO Balance": neo_val + " NEO", "GAS Balance": gas_val.toFixed(8) + " GAS", "Balance Data": balance });
        }).catch(err => {
            console.log(err)
            resp.status(400).json({ Error: err });
        });
    })
});

app.get('/BTC/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    BTC_explorer_config().then(ins => {
        ins.getUnspentUtxos(req.params.key, function (err, utxos) {
            // console.log(utxos);
            let balance = 0;
            for (var i = 0; i < utxos.length; i++) {
                balance += utxos[i]['satoshis'];
            }
            console.log({ "Account": req.params.key, "BTC Balance": balance / 100000000 + " BTC" });
            resp.status(200).json({ "Account": req.params.key, "BTC Balance": balance / 100000000 + " BTC" });
        })
    })
});

app.get('/LTC/:key', function (req, resp) {

    if (!req.params.key)
        resp.status(400).json({ Error: "Bad Request" });

    LTC_explorer_config().then(ins => {
        ins.getUtxos(req.params.key, function (err, utxos) {
            // console.log(utxos);
            let balance = 0;
            for (var i = 0; i < utxos.length; i++) {
                balance += utxos[i]['satoshis'];
            }
            console.log({ "Account": req.params.key, "LTC Balance": balance / 100000000 + " LTC" });
            resp.status(200).json({ "Account": req.params.key, "LTC Balance": balance / 100000000 + " LTC" });
        })
    })
});


app.all('*', function (req, resp) {
    resp.status(404).json({ Error: "Sorry, this is an invalid URL." });
});

// -------------- Set Config ----------------

ETH_config = () => new Promise(function (resolve, reject) {
    const Provider = 'https://ropsten.infura.io/Vr1GWcLG0XzcdrZHWMPu'           // For testnet
    // const Provider = 'https://mainnet.infura.io/Vr1GWcLG0XzcdrZHWMPu'        // For Mainnet
    const web3 = new Web3(new Web3.providers.HttpProvider(Provider));
    resolve(web3);
})

TRON_config = () => new Promise(function (resolve, reject) {
    const HttpProvider = TronWeb.providers.HttpProvider;
    const URL = 'https://api.shasta.trongrid.io';       // For Testnet
    // const URL = 'https://api.trongrid.io';           // For Mainnet

    const fullNode = new HttpProvider(URL); // Full node http endpoint
    const solidityNode = new HttpProvider(URL); // Solidity node http endpoint
    const eventServer = new HttpProvider(URL); // Contract events http endpoint
    const tronWeb = new TronWeb(fullNode, solidityNode, eventServer); // Config
    // const tronWeb = new TronWeb(fullNode, solidityNode, eventServer, privateKey); // Config with private key

    resolve(tronWeb);
})

EOS_config = (pKey, acc) => new Promise(function (resolve, reject) {
    eos = Eos({
        keyProvider: pKey,
        httpEndpoint: 'http://jungle2.cryptolions.io:80',
        chainId: 'e70aaab8997e1dfce58fbfac80cbbb8fecec7b99cf982a9444273cbc64c41473',        // For Testnet
        // httpEndpoint: 'https://api.eosnewyork.io:443',
        // chainId: 'aca376f206b8fc25a6ed44dbdc66547c36c6c33e3a119ffbeaef943642f0e906',     // For mainnet
        authorization: acc + '@active',
        broadcast: true,
        debug: true,
        sign: true
    })
    resolve(eos);
})

XLM_config = () => new Promise(function (resolve, reject) {
    StellarSdk.Network.useTestNetwork();
    const server = new StellarSdk.Server('https://horizon-testnet.stellar.org');    // For Testnet
    // const server = new StellarSdk.Server('https://horizon.stellar.org/');        // For Mainnet
    resolve(server);
})

XRP_config = () => new Promise(function (resolve, reject) {
    const api = new RippleAPI({ server: 'wss://s.altnet.rippletest.net:51233' });   // For Testnet
    // const api = new RippleAPI({ server: 'wss://s1.ripple.com' });                // For Mainnet
    resolve(api);
})

NEO_config = () => new Promise(function (resolve, reject) {
    const network = "TestNet";
    // const network = "MainNet";
    const apiData = new api.neoscan.instance(network);
    resolve(apiData);
})

// Same config for BTC and LTC
BTC_config = () => new Promise((resolve, reject) => {
    delete global._bitcore
    var bitcore = require("bitcore-lib");
    var bitJS = {
        "bitcore": bitcore,
        "trx": new bitcore.Transaction()
    }
    resolve(bitJS);
})

BTC_explorer_config = () => new Promise((resolve, reject) => {
    delete global._bitcore
    var Insight = require("bitcore-explorers").Insight;
    var insight = new Insight("testnet");           // For testnet
    // var insight = new Insight();                 // For mainnet
    resolve(insight);
})

LTC_explorer_config = () => new Promise((resolve, reject) => {
    delete global._litecore
    var Insight = require("litecore-explorers").Insight;
    var insight = new Insight("http://explorer.litecointools.com");             // For testnet 
    // var insight = new Insight('https://insight.litecore.io', 'mainnet');     // For mainnet
    resolve(insight);
})

// ------------------------------------------

app.listen(process.env.PORT || 3030, () => console.log("API run in 3030 port"));